import map from './map';

export default (a, b) => map(Array(a), b);