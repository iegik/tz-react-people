import fetch from '../mocks';

export default (...args) => {
  try {
    return fetch(...args)
  } catch (e) {
    return Promise.reject({errors:[e]});
  }
};
