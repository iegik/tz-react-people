import ru_RU from '../i18n/ru_RU.json';
const texts = {
  ru_RU: ru_RU,
}
const i18n = lang => (text, n) => texts[lang][text][0]; // TODO: plural formula
const _ =  i18n('ru_RU');
export default ([a,b], n) => b ? _(a + '%s' + b, n).replace(/%s/, n) : _(a);