export default (arr = [], fn, out = []) => {
  for (let i = 0; i < arr.length; i++){
    out = fn(out, arr[i], i, arr);
  }
  return out;
};