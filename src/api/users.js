import request from '../utils/request';

const {assign} = Object;
const api = (url, options) => body => request(url, assign({}, options, {body}));

const usersAPI = (id, body) => api('http://localhost:3000/users' + (id != null ? `/${id}` : ''), {
  method: 'POST',
  headers: {'Content-Type': 'application/json'},
})(JSON.stringify(body)).then(res => {
  if (res.ok) {
    return res.body && res.json()
  } else {
    return Promise.reject({code: res.status, message: res.statusText})
  }
});

export const requestAll = () => usersAPI();
export const requestAllByName = () => usersAPI(null, {sortBy: 'firstname'});
export const requestSave = (user) => usersAPI(user.id, user);
export const requestDelete = (user) => usersAPI(user.id, 'delete');