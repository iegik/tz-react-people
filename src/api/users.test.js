import fetchMock from 'fetch-mock';
import {requestAll, requestAllByName} from './users';
import users from '../mocks/users';
import usersByName from '../mocks/usersByName';

it('fetches all users', () => {
  const fixture = JSON.stringify(users)
  fetchMock.post('*', fixture);
  requestAll().then(function(data) {
    expect(fixture).toEqual(JSON.stringify(data))
  });
  fetchMock.restore();
})

xit('fetches sorted by firstname', () => {
  const fixture = JSON.stringify(usersByName)
  fetchMock.post('*', fixture);
  requestAllByName().then(function(data) {
    expect(fixture).toEqual(JSON.stringify(data))
  });
  fetchMock.restore();
})

