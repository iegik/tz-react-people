import React from 'react';
import map from '../utils/map';
import _ from '../utils/gettext';
import Table, {
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Tooltip from 'material-ui/Tooltip';

// first name, last name, возраст, email
export default (props = {}) => {
  let {users, sortedBy, onSortByFirstname, selectUser} = props;
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>
            <Tooltip title="Sort" placement="bottom-start" enterDelay={300}>
              <TableSortLabel {...{
                direction: 'asc',
                active: sortedBy === 'firstname',
                onClick: onSortByFirstname,
              }}>
                {_ `First Name`}
              </TableSortLabel>
            </Tooltip>
          </TableCell>
          <TableCell>{_`Last Name`}</TableCell>
          <TableCell>{_`Age`}</TableCell>
          <TableCell>{_`E-mail`}</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {map(users, (user, key) => {
          return (
            <TableRow {...{
              key,
              onClick: () => selectUser(user),
            }}>
              <TableCell>{user.firstname}</TableCell>
              <TableCell>{user.lastname}</TableCell>
              <TableCell>{user.age}</TableCell>
              <TableCell>{user.email}</TableCell>
            </TableRow>
          )
        })}
      </TableBody>
    </Table>
  )
}