import React from 'react';
import _ from '../utils/gettext';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';

// first name, last name, возраст, email
export default (props = {}) => {
  let {
    user,
    onChangeFirstname,
    onChangeLastname,
    onChangeAge,
    onChangeEmail,
    onSave,
    errors,
    saved,
    deleted,
    onDelete,
  } = props;
  return (
    <form noValidate autoComplete="off">
      <TextField {...{
        id: 'firstname',
        error: !!errors['firstname'],
        label: _ `First Name`,
        helperText: errors['firstname'],
        value: user.firstname,
        onChange: onChangeFirstname,
      }}/>
      <br />
      <TextField {...{
        id: 'lastname',
        error: !!errors['lastname'],
        label: (!!errors['lastname'] ? errors['lastname'] : _ `Last Name`),
        value: user.lastname,
        onChange: onChangeLastname,
      }}/>
      <br />
      <TextField {...{
        id: 'age',
        error: !!errors['age'],
        label: (!!errors['age'] ? errors['age'] : _ `Age`),
        value: user.age,
        onChange: onChangeAge,
      }}/>
      <br />
      <TextField {...{
        id: 'email',
        error: !!errors['email'],
        label: (!!errors['email'] ? errors['email'] : _ `E-mail`),
        value: user.email,
        onChange: onChangeEmail,
      }}/>
      <br />
      <Button raised onClick={onDelete} disabled={deleted}>
        {_ `Delete`}
      </Button>
      <Button raised color="primary" onClick={onSave} disabled={saved}>
        {_ `Save`}
      </Button>
    </form>
  )
}