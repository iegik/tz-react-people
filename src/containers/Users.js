import React, { Component } from 'react';
import PropTypes  from 'prop-types';
import UserList from '../components/UserList';
import EditUser from '../containers/EditUser';
import Paper from 'material-ui/Paper';
import {requestAll, requestAllByName} from '../api/users';
import map from '../utils/map';
import reduce from '../utils/reduce';

class App extends Component {
  state = {
    users: this.props.users,
    current: null,
    errors: {},
  }

  async componentDidMount() {
    try {
      let users = await requestAll();
      this.setState({users})
    } catch (e) {
      this.setState({errors: e.errors})
    }
  }

  async sortByName() {
    try {
      this.setState({loading: true, sortBy: 'firstname'})
      let users = await requestAllByName();
      this.setState({users, loading: false})
    } catch (e) {
      this.setState({errors: {...this.state.errors, ...e.errors}})
    }
  }

  selectUser(current) {
    this.setState({current});
  }

  updateUser(current) {
    this.setState({users: map(this.state.users, user => user.id === current.id ? current : user)});
  }

  deleteUser(current) {
    let users = reduce(this.state.users, (users, user, i) => {
      if(user.id !== current.id) {
        users.push(user);
      }
      return users;
    }, []);
    this.setState({users, current: null});
  }

  renderUserForm = (current) => (
    <EditUser {...{
      user: current,
      errors: this.state.errors,
      onSave: user => this.updateUser(user),
      onDelete: user => this.deleteUser(user),
    }}/>
  );

  renderUserList = (users) => UserList({
    users,
    errors: this.state.errors,
    onSortByFirstname: () => this.sortByName(),
    selectUser: (user) => this.selectUser(user),
  });

  render() {
    let {users, current} = this.state;
    return (
      <Paper>
        { current ? this.renderUserForm(current) : null }
        { users ? this.renderUserList(users) : null }
      </Paper>
    )
  }
}
App.defaultProps = {
  users: PropTypes.array.Required,
}
export default App;