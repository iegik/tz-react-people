import { Component } from 'react';
import PropTypes  from 'prop-types';
import UserForm from '../components/UserForm';
import {requestSave, requestDelete} from '../api/users';

class App extends Component {
  state = {
    user: this.props.user,
    errors: this.props.errors,
    saved: true,
    deleted: false,
  }

  changeUserData = field => event => {
    this.setState({ user: {...this.state.user, [field]: event.target.value }, saved: false});
  }

  render() {
    let {user, errors, saved, deleted} = this.state;
    return UserForm({
      user,
      errors,
      saved,
      deleted,
      onChangeFirstname: this.changeUserData('firstname'),
      onChangeLastname: this.changeUserData('lastname'),
      onChangeAge: this.changeUserData('age'),
      onChangeEmail: this.changeUserData('email'),
      onSave: () => (async () => {
        try {
          this.setState({loading: true})
          await requestSave(user);
          this.props.onSave(user)
          this.setState({saved: true, loading: false})
        } catch (e) {
          this.setState({errors: {...this.state.errors, ...e.errors}})
        }
      })(),
      onDelete: () => (async () => {
        try {
          this.setState({loading: true})
          await requestDelete(user);
          this.props.onDelete(user)
          this.setState({deleted: true, loading: false})
        } catch (e) {
          this.setState({errors: {...this.state.errors, ...e.errors}})
        }
      })(),
    });
  }
}
App.defaultProps = {
  user: PropTypes.object.Required,
}
export default App;