import fetchMock from 'fetch-mock';
import users from '../mocks/users';
import usersByName from '../mocks/usersByName';
const fetch = fetchMock.sandbox();

fetch.post((url, ops) => {
  let {body} = ops;
  return url === 'http://localhost:3000/users' && !body
}, users);

fetch.post((url, ops) => {
  let {body} = ops;
  return url === 'http://localhost:3000/users' && !!body
}, usersByName);

fetch.post((url, ops) => {
  let {body} = ops;
  return /http:\/\/localhost:3000\/users\/.*/.test(url) && !!body
}, 200);

export default fetch;