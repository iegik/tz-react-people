import users from './users';
import {sortBy} from 'lodash';

export default sortBy(users, ['firstname'])