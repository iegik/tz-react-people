import faker from 'faker';
import matrix from '../utils/matrix';

export default matrix(10, () => ({
  id: faker.random.uuid(),
  firstname: faker.name.firstName(),
  lastname: faker.name.lastName(),
  email: faker.internet.email(),
  age: faker.random.number(50) + 16,
}))
