import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Users from './containers/Users';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { createMuiTheme } from 'material-ui/styles';
import { purple, green } from 'material-ui/colors';

const theme = createMuiTheme({
  palette: {
    primary: purple,
    secondary: green,
  },
});

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Table</h1>
        </header>
        <MuiThemeProvider {...{theme}}>
          <Users/>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
